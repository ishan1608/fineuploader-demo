from django.conf.urls import url
from .views import IndexPage, upload
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    url(r'^$', IndexPage.as_view(), name='index'),
    url(r'^upload$', upload, name='index'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
