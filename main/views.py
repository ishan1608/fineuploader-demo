from django.views.generic import TemplateView
from django.http.response import HttpResponse
from .models import ProfilePicture
import json


class IndexPage(TemplateView):
    template_name = 'main/index.html'

    def get_context_data(self, **kwargs):
        return {
            'pictures': ProfilePicture.objects.all()
        }

    def get(self, request, *args, **kwargs):
        return super(IndexPage, self).get(request, *args, **kwargs)


def upload(request):
    if request.method == 'POST':
        profile_picture = ProfilePicture()
        profile_picture.picture = request.FILES['picture_file']
        profile_picture.save()
        data = {
            'url': profile_picture.picture.url,
            "success": True
        }
        return HttpResponse(json.dumps(data), content_type='application/json', status=201)
    return HttpResponse(status=405)
