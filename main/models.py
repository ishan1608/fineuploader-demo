from __future__ import unicode_literals

from django.db import models


class ProfilePicture(models.Model):
    picture = models.FileField(upload_to='uploads/')

    def __str__(self):
        return u'{} - {}'.format(self.id, self.picture.name)
